<?php
namespace App\Form;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SettingsForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction('save')
            ->add('smtp_host', TextType::class, ['required' => false])
            ->add('email', TextType::class, ['required' => false, 'label' => 'email or login'])
            ->add('password', PasswordType::class, ['required' => false])
            ->add('smtp_port', TextType::class, ['required' => false])
            ->add('administrator_email', EmailType::class, ['required' => false])
            ->add('save', SubmitType::class);
    }
}