<?php

namespace App\Admin;

use App\Entity\Page;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

/**
 * Created by PhpStorm.
 * User: Applebred
 * Date: 14.11.2019
 * Time: 11:45
 */
final class ArticleAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class);
        $formMapper->add('content', TextareaType::class);
        $formMapper->add('page', EntityType::class, ['class' => Page::class, 'choice_label' => function(Page $page) {
            return $page->getName();
        }]);
        $formMapper->add('is_script', CheckboxType::class, ['required' => false, 'label' => 'Поместить в начало страницы (для скриптов yandex.metrika и тд...)']);
        $formMapper->add('published', CheckboxType::class, ['required' => false]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
        $datagridMapper->add('published');
        $datagridMapper->add('updated_date');
        $datagridMapper->add('created_date');
        $datagridMapper->add('page.name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
        $listMapper->add('published');
        $listMapper->add('updated_date');
        $listMapper->add('created_date');
        $listMapper->add('page.name');
        $listMapper->add('_action', 'actions', [
            'actions' => [
                'edit' => [],
                'delete' => [],
            ]
        ]);
    }

    public function preUpdate($article)
    {
        $date = new \DateTime('now');
        $article->setUpdatedDate($date);
    }

    /**
     * @param object $article
     */
    public function preValidate($article)
    {
        $date = new \DateTime('now');
        $article->setCreatedDate($date);
    }
}