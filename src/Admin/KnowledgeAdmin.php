<?php

namespace App\Admin;

use App\Entity\Category;
use App\Entity\Knowledge;
use App\Entity\Settings;
use App\Entity\Subscriber;
use App\Form\KnowledgeType;
use Doctrine\ORM\EntityManager;
use Sonata\FormatterBundle\Form\Type\FormatterType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilder;
use App\Controller\MailController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

/**
 * Created by PhpStorm.
 * User: Applebred
 * Date: 14.11.2019
 * Time: 11:45
 */
final class KnowledgeAdmin extends AbstractAdmin
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Environment
     */
    private $twig;

    public function setEntityManager (EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function setMailTemplate(Environment $twig) {
        $this->twig = $twig;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('heading', TextType::class);
        $formMapper->add('content', SimpleFormatterType::class, [
            'format' => 'richhtml',
        ]);
        $formMapper->add('category_id', EntityType::class, ['class' => Category::class, 'choice_label' => function (Category $category) {
            return $category->getHeading();
        }]);
        $formMapper->add('file', FileType::class, ['required' => false, 'label' => 'small image']);
        $formMapper->add('big_file', FileType::class, ['required' => false, 'label' => 'big image']);
        $formMapper->add('published_date', DateTimeType::class, ['required' => false]);
        $formMapper->add('published', CheckboxType::class, ['required' => false]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('heading');
        $datagridMapper->add('published');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('heading');
        $listMapper->add('created_at');
        $listMapper->add('updated_at');
        $listMapper->add('published');
        $listMapper->add('category_id.heading');
        $listMapper->add('_action', 'actions', [
            'actions' => [
                'edit'   => [],
                'delete' => [],
            ]
        ]);
    }

    /**
     * @param Knowledge $knowledge
     *
     */
    public function prePersist($knowledge)
    {
        $this->manageFileUpload($knowledge);
        $isNew = !$knowledge->getId();


        if ($knowledge->getPublished() && $isNew) {
            $this->SendMail($knowledge);
        }
    }

    /**
     * @param Knowledge $knowledge
     *
     */
    public function preUpdate($knowledge)
    {
        $date = new \DateTime('now');
        $knowledge->setUpdatedAt($date);
        $this->manageFileUpload($knowledge);
    }

    /**
     * @param Knowledge $category
     */
    public function preValidate($knowledge)
    {
        $date = new \DateTime('now');
        $knowledge->setCreatedAt($knowledge->getCreatedAt() ? $knowledge->getCreatedAt() : $date);
    }

    /**
     * @param Knowledge $knowledge
     *
     */
    private function manageFileUpload($knowledge)
    {
        if ($knowledge->getFile()) {
            $knowledge->refreshUpdated();
        }

        if ($knowledge->getBigFile()) {
            $knowledge->refreshUpdated();
        }
    }

    /**
     * @param Knowledge $knowledge
     */
    public function SendMail($knowledge):void
    {

        /**
         * @var EntityManager $em
         */
        $em = $this->entityManager;

        $settingsRepository = $em->getRepository(Settings::class);

        $settings = $settingsRepository->findOneBy([]);


        $yandexSmtpHost   = $settings->getSmtpHost();
        $yandexEmail      = $settings->getEmail();
        $yandexPassword   = $settings->getPassword();
        $yandexSmtpPort   = $settings->getSmtpPort();
        $yandexEncryption = 'SSL';

        $subscriberRepository = $em->getRepository(Subscriber::class);

        /**
         * @var Subscriber[] $subscribers
         */
        $subscribers = $subscriberRepository->findAll();

        foreach ($subscribers as $subscriber) {
            $targetEmail = $subscriber->getEmail();
            $subject = 'Привет ' . $subscriber->getName() . '. Новое знание на тему: ' . $knowledge->getHeading();
            $body    = $this->twig->render('email/subscribe.twig', ['knowledge' => $knowledge, 'subscriber' => $subscriber]);

            $transport = (new Swift_SmtpTransport($yandexSmtpHost, $yandexSmtpPort))
                ->setUsername($yandexEmail)
                ->setPassword($yandexPassword)
                ->setEncryption($yandexEncryption);

            $mailer = new Swift_Mailer($transport);

            $message = (new Swift_Message($subject))
                ->setFrom([$yandexEmail => $yandexEmail])
                ->setTo([$targetEmail])
                ->setBody($body, 'text/html');

            $mailer->send($message);
        }
    }
}