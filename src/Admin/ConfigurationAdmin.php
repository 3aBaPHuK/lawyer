<?php

namespace App\Admin;

use App\Entity\Page;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;

/**
 * Created by PhpStorm.
 * User: Applebred
 * Date: 14.11.2019
 * Time: 11:45
 */
final class ConfigurationAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'settings';
    protected $baseRouteName = 'settings';

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
        $collection->add('save');
    }
}