<?php

namespace App\Admin;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

/**
 * Created by PhpStorm.
 * User: Applebred
 * Date: 14.11.2019
 * Time: 11:45
 */
final class ContactAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('email', EmailType::class);
        $formMapper->add('phoneFirst', TextType::class, ['required' => false]);
        $formMapper->add('phoneSecond', TextType::class, ['required' => false]);
        $formMapper->add('address', TextType::class, ['required' => false]);
        $formMapper->add('busStation', TextType::class, ['required' => false]);
        $formMapper->add('busNumbers', TextType::class, ['required' => false]);
        $formMapper->add('tramNumbers', TextType::class, ['required' => false]);
        $formMapper->add('active', CheckboxType::class, ['required' => false]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('email');
        $datagridMapper->add('active');
        $datagridMapper->add('phoneFirst');
        $datagridMapper->add('phoneSecond');
        $datagridMapper->add('address');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('email');
        $listMapper->addIdentifier('active');
        $listMapper->addIdentifier('phoneFirst');
        $listMapper->addIdentifier('phoneSecond');
    }
}