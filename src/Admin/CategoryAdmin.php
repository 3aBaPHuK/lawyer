<?php

namespace App\Admin;

use App\Entity\Category;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

/**
 * Created by PhpStorm.
 * User: Applebred
 * Date: 14.11.2019
 * Time: 11:45
 */
final class CategoryAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('heading', TextType::class);
        $formMapper->add('description', TextareaType::class);
        $formMapper->add('published', CheckboxType::class, ['required' => false]);
        $formMapper->add('file', FileType::class, ['required' => false]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('heading');
        $datagridMapper->add('type', 'doctrine_orm_class', ['sub_classes' => $this->getSubClasses()]);

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('heading');
        $listMapper->add('created_at');
        $listMapper->add('updated_at');
        $listMapper->add('published');
        $listMapper->add('_action', 'actions', [
            'actions' => [
                'edit' => [],
                'delete' => []
            ]
        ]);
    }

    /**
     * @param object $category
     */
    public function prePersist($category)
    {
        $this->manageFileUpload($category);
    }

    /**
     * @param object $category
     */
    public function preUpdate($category)
    {
        $date = new \DateTime('now');
        $category->setUpdatedAt($date);
        $this->manageFileUpload($category);
    }

    /**
     * @param object $category
     */
    private function manageFileUpload($category)
    {
        if ($category->getFile()) {
            $category->refreshUpdated();
        }
    }

    /**
     * @param Category $category
     */
    public function preValidate($category)
    {
        $date = new \DateTime('now');
        $category->setCreatedAt($category->getCreatedAt() ? $category->getCreatedAt() : $date);
    }
}