<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SettingsRepository")
 */
class Settings
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $smtp_host;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $administrator_email;

    /**
     * @ORM\Column(type="string", length=11, nullable=true)
     */
    private $smtp_port;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSmtpHost(): ?string
    {
        return $this->smtp_host;
    }

    public function setSmtpHost(?string $smtp_host): self
    {
        $this->smtp_host = $smtp_host;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getAdministratorEmail(): ?string
    {
        return $this->administrator_email;
    }

    public function setAdministratorEmail(?string $administrator_email): self
    {
        $this->administrator_email = $administrator_email;

        return $this;
    }

    public function getSmtpPort(): ?string
    {
        return $this->smtp_port;
    }

    public function setSmtpPort(?string $smtp_port): self
    {
        $this->smtp_port = $smtp_port;

        return $this;
    }
}
