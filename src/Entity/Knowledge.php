<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\KnowledgeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Knowledge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Unmapped property to handle file uploads
     */
    private $file;

    /**
     * Unmapped property to handle file uploads
     */
    private $bigFile;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image_url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $heading;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="knowledge")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category_id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $big_image_url;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $published_date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->image_url;
    }

    public function setImageUrl(string $image_url): self
    {
        $this->image_url = $image_url;

        return $this;
    }

    public function getHeading(): ?string
    {
        return $this->heading;
    }

    public function setHeading(?string $heading): self
    {
        $this->heading = $heading;

        return $this;
    }

    public function getCategoryId(): ?Category
    {
        return $this->category_id;
    }

    public function setCategoryId(?Category $category_id): self
    {
        $this->category_id = $category_id;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function upload()
    {
        if (null !== $this->getBigFile()) {
            $this->getBigFile()->move(
                $this->getUploadRootDir(),
                'big_' . $this->getBigFile()->getClientOriginalName()
            );

            $this->setBigImageUrl('big_' . $this->getBigFile()->getClientOriginalName());
            $this->setBigFile(null);
        }

        if (null !== $this->getFile()) {
            $this->getFile()->move(
                $this->getUploadRootDir(),
                $this->getFile()->getClientOriginalName()
            );

            $this->setImageUrl($this->getFile()->getClientOriginalName());
            $this->setFile(null);
        }
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function lifecycleFileUpload() {
        $this->upload();
    }

    /**
     * Updates the hash value to force the preUpdate and postUpdate events to fire
     */
    public function refreshUpdated() {
        $date = new \DateTime('now');
        $this->setUpdatedAt($date);
    }

    /**
     * @return mixed
     */
    public function getBigFile()
    {
        return $this->bigFile;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     * @return $this
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @param mixed $file
     * @return $this
     */
    public function setBigFile($bigFile)
    {
        $this->bigFile = $bigFile;

        return $this;
    }

    protected function getUploadRootDir()
    {
        $path = realpath(__DIR__.DIRECTORY_SEPARATOR .'..'.DIRECTORY_SEPARATOR .'..'.DIRECTORY_SEPARATOR.$this->getUploadDir());
        return $path;
    }

    protected function getUploadDir()
    {
        return 'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'knowledge';
    }

    public function getBigImageUrl(): ?string
    {
        return $this->big_image_url;
    }

    public function setBigImageUrl(?string $big_image_url): self
    {
        $this->big_image_url = $big_image_url;

        return $this;
    }

    public function getPublishedDate(): ?\DateTimeInterface
    {
        return $this->published_date;
    }

    public function setPublishedDate(?\DateTimeInterface $published_date): self
    {
        $this->published_date = $published_date;

        return $this;
    }
}
