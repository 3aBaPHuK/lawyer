<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactsRepository")
 */
class Contacts
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phoneFirst;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phoneSecond;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $busStation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $busNumbers;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tramNumbers;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneFirst(): ?string
    {
        return $this->phoneFirst;
    }

    public function setPhoneFirst(?string $phoneFirst): self
    {
        $this->phoneFirst = $phoneFirst;

        return $this;
    }

    public function getPhoneSecond(): ?string
    {
        return $this->phoneSecond;
    }

    public function setPhoneSecond(?string $phoneSecond): self
    {
        $this->phoneSecond = $phoneSecond;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getBusStation(): ?string
    {
        return $this->busStation;
    }

    public function setBusStation(?string $busStation): self
    {
        $this->busStation = $busStation;

        return $this;
    }

    public function getBusNumbers(): ?string
    {
        return $this->busNumbers;
    }

    public function setBusNumbers(?string $busNumbers): self
    {
        $this->busNumbers = $busNumbers;

        return $this;
    }

    public function getTramNumbers(): ?string
    {
        return $this->tramNumbers;
    }

    public function setTramNumbers(?string $tramNumbers): self
    {
        $this->tramNumbers = $tramNumbers;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
