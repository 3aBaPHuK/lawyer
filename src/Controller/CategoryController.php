<?php

namespace App\Controller;

use App\Entity\Category;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Knowledge controller.
 * @Route("/api", name="api_")
 */
class CategoryController extends FOSRestController
{
    /**
     * Lists all Articles.
     * @Rest\Get("/categories")
     *
     * @return Response
     */
    public function getArticlesAction(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Category::class);
        $category  = $repository->findall();

        return $this->handleView($this->view($category));
    }

    /**
     * Lists all Articles.
     * @Rest\Get("/categories/{id}")
     *
     * @param $id
     * @return Response
     */
    public function getArticleAction($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Category::class);

        /**
         * @var Category $article
         */
        $category = $repository->find($id);

        if (!$category) {
            return $this->handleView($this->view(['status' => 'NOT_FOUND'], Response::HTTP_NOT_FOUND));
        }

        return $this->handleView($this->view($category));
    }
}