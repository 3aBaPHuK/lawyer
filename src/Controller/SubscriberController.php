<?php

namespace App\Controller;

use App\Entity\Subscriber;
use App\Form\SubscriberType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Article controller.
 * @Route("/api", name="api_")
 */
class SubscriberController extends FOSRestController
{
    /**
     * Subscribe.
     * @Rest\Post("/subscribe")
     *
     * @param Request $request
     * @return Response
     */
    public function SubscribeAction(Request $request): Response
    {
        $subscriber           = new Subscriber();
        $form                 = $this->createForm(SubscriberType::class, $subscriber);
        $data                 = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($subscriber);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }

        return $this->handleView($this->view($form->getErrors()));
    }
}