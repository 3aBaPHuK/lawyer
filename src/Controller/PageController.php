<?php
/**
 * Created by PhpStorm.
 * User: Applebred
 * Date: 02.11.2019
 * Time: 19:42
 */

namespace App\Controller;


use App\Entity\Page;
use App\Form\PageType;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Page controller.
 * @Route("/api", name="api_")
 */
class PageController extends FOSRestController
{
    /**
     * Lists all Pages.
     * @Rest\Get("/pages")
     *
     * @return Response
     */
    public function getPagesAction(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Page::class);
        $pages      = $repository->findall();
        return $this->handleView($this->view($pages));
    }

    /**
     * Get Page.
     * @Rest\Get("/pages/{id}")
     *
     * @return Response
     */
    public function getPageAction($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Page::class);

        /**
         * @var Page $page
         */
        $page = $repository->find($id);

        if (!$page) {
            return $this->handleView($this->view(['status' => 'NOT_FOUND'], Response::HTTP_NOT_FOUND));
        }

        return $this->handleView($this->view($page));
    }
}