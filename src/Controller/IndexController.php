<?php
/**
 * Created by PhpStorm.
 * User: Applebred
 * Date: 02.11.2019
 * Time: 17:27
 */

namespace App\Controller;


use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IndexController extends AbstractController
{
    public function indexAction()
    {
        $repository      = $this->getDoctrine()->getRepository(Article::class);
        $serviceArticles = $repository->findBy(['is_script' => true]);

        $response = $this->render('index/index.twig', ['scripts' => $serviceArticles]);
        $response->headers->set('Content-Type', 'text/html');

        return $response;
    }
}