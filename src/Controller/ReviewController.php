<?php
/**
 * Created by PhpStorm.
 * User: Applebred
 * Date: 04.11.2019
 * Time: 20:29
 */

namespace App\Controller;

use App\Entity\Review;
use App\Form\ReviewType;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Review controller.
 * @Route("/api", name="api_")
 */
class ReviewController extends FOSRestController
{
    /**
     * List All Reviews
     * @Rest\Get("/reviews")
     */
    public function getReviewsAction(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Review::class);
        $reviews    = $repository->findAll();

        return $this->handleView($this->view($reviews));
    }

    /**
     * Get review by id
     * @Rest\Get("/reviews/{id}")
     *
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function getReviewAction($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Review::class);
        $review     = $repository->find($id);

        if (!$review) {
            return $this->handleView($this->view(['Error' => 'NOT_FOUND'], Response::HTTP_NOT_FOUND));
        }

        return $this->handleView($this->view($review));
    }
}