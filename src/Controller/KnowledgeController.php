<?php

namespace App\Controller;

use App\Entity\Knowledge;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Knowledge controller.
 * @Route("/api", name="api_")
 */
class KnowledgeController extends FOSRestController
{
    /**
     * Lists all Articles.
     * @Rest\Get("/knowledge")
     *
     * @return Response
     */
    public function getArticlesAction(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Knowledge::class);
        $knowledge  = $repository->findall();

        return $this->handleView($this->view($knowledge));
    }

    /**
     * Lists all Articles.
     * @Rest\Get("/knowledge/{id}")
     *
     * @param $id
     * @return Response
     */
    public function getArticleAction($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Knowledge::class);

        /**
         * @var Knowledge $article
         */
        $knowledge = $repository->find($id);

        if (!$knowledge) {
            return $this->handleView($this->view(['status' => 'NOT_FOUND'], Response::HTTP_NOT_FOUND));
        }

        return $this->handleView($this->view($knowledge));
    }
}