<?php
/**
 * Created by PhpStorm.
 * User: Applebred
 * Date: 01.12.2019
 * Time: 18:49
 */

namespace App\Controller;


use App\Entity\Settings;
use App\Form\SettingsForm;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class AdminConfigurationController extends CRUDController
{
    public function listAction()
    {
        $repository = $this->getDoctrine()->getRepository(Settings::class);

        $settings = $repository->findAll();
        $singleSettings = array_shift($settings);

        $form = $this->createForm(SettingsForm::class, $singleSettings);

        return $this->renderWithExtraParams('admin/settings.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     */
    public function saveAction(Request $request) {
        $data = $request->get('settings_form');
        $repository = $this->getDoctrine()->getRepository(Settings::class);

        $settings = $repository->findAll();

        if (!$settings) {
            $singleSettings = new Settings();
        } else {
            $singleSettings = array_shift($settings);
        }

        $form = $this->createForm(SettingsForm::class, $singleSettings, ['allow_extra_fields' => true])->add('save',SubmitType::class);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($singleSettings);
            $em->flush();

            return $this->redirectToRoute('sonata_admin_dashboard');
        }
    }
}