<?php

namespace App\Controller;

use App\Entity\Settings;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Mail controller.
 * @Route("/api", name="api_")
 */
class MailController extends FOSRestController
{
    /**
     * send mail.
     * @Rest\Post("/mail")
     *
     * @return Response
     */
    public function SendMail(Request $request): Response
    {
        $repository = $this->getDoctrine()->getRepository(Settings::class);

        $settings = $repository->findOneBy([]);


        $data = json_decode($request->getContent(), true);
        $yandexSmtpHost = $settings->getSmtpHost();
        $yandexEmail = $settings->getEmail();
        $yandexPassword = $settings->getPassword();
        $yandexSmtpPort = $settings->getSmtpPort();
        $yandexEncryption = 'SSL';

        $targetEmail = $settings->getAdministratorEmail();

        $transport = (new Swift_SmtpTransport($yandexSmtpHost, $yandexSmtpPort))
            ->setUsername($yandexEmail)
            ->setPassword($yandexPassword)
            ->setEncryption($yandexEncryption);

        $mailer = new Swift_Mailer($transport);

        $message = (new Swift_Message($data['subject']))
            ->setFrom([$yandexEmail => $yandexEmail])
            ->setTo([$targetEmail])
            ->setBody($data['message']);


        $result = $mailer->send($message);

        return $this->handleView($this->view($result ? ['message' => 'ok'] : ['message' => 'fail'], $result ? Response::HTTP_OK : Response::HTTP_FORBIDDEN));
    }
}