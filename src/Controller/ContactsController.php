<?php
/**
 * Created by PhpStorm.
 * User: Applebred
 * Date: 14.11.2019
 * Time: 11:06
 */

namespace App\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Contacts;

/**
 * Address controller.
 * @Route("/api", name="api_")
 */
class ContactsController extends FOSRestController
{
    /**
     * Lists all Articles.
     * @Rest\Get("/contacts")
     *
     * @return Response
     */
    public function getContactsAction()
    {
        $repository = $this->getDoctrine()->getRepository(Contacts::class);
        $contacts   = $repository->findAll();

        return $this->handleView($this->view($contacts));
    }

    /**
     * Get active contact.
     * @Rest\Get("/contacts/active")
     *
     * @return Response
     */
    public function getActiveContactAction()
    {
        $repository    = $this->getDoctrine()->getRepository(Contacts::class);
        $activeContact = $repository->findOneBy(['active' => true]);

        return $this->handleView($this->view($activeContact));
    }
}