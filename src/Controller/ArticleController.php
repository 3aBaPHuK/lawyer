<?php

namespace App\Controller;

use App\Entity\Page;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Article;
use App\Form\ArticleType;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Article controller.
 * @Route("/api", name="api_")
 */
class ArticleController extends FOSRestController
{
    /**
     * Lists all Articles.
     * @Rest\Get("/articles")
     *
     * @return Response
     */
    public function getArticlesAction(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Article::class);
        $articles   = $repository->findall();

        return $this->handleView($this->view($articles));
    }

    /**
     * Lists all Articles.
     * @Rest\Get("/articles/{id}")
     *
     * @return Response
     */
    public function getArticleAction($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Article::class);

        /**
         * @var Article $article
         */
        $article = $repository->find($id);

        if (!$article) {
            return $this->handleView($this->view(['status' => 'NOT_FOUND'], Response::HTTP_NOT_FOUND));
        }

        return $this->handleView($this->view($article));
    }
}