export default {
    state: {
        contacts: null
    },
    mutations: {
        fetchContacts(state) {
            fetch('/api/contacts/active').then(response => response.json()).then(data => {
                state.contacts = data;
            });
        }
    },
    actions: {},
    getters: {}
}