export default {
    state: {
        reviews: null
    },
    mutations: {
        fetchReviews(state) {
            fetch('/api/reviews').then(response => response.json()).then(data => {
                state.reviews = data;
            });
        }
    },
    actions: {},
}