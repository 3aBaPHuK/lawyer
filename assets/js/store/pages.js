export default {
    state: {
        pages: null
    },
    mutations: {
        fetchPages(state) {
            fetch('/api/pages').then(response => response.json()).then(data => {
                state.pages = data;
            });
        }
    },
    actions: {},
}