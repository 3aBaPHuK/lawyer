export default {
    state: {
        categories: null
    },
    mutations: {
        fetchCategories(state) {
            fetch('/api/categories').then(response => response.json()).then(data => {
                state.categories = data;
            });
        }
    },
    actions: {},
    getters: {}
}