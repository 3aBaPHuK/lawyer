export default {
    state: {
        articles: null
    },
    mutations: {
        fetchArticles(state) {
            fetch('/api/articles').then(response => response.json()).then(data => {
                state.articles = data;
            });
        }
    },
    actions: {},
    getters: {}
}