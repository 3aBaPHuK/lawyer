export default {
    state: {
        knowledge: null
    },
    mutations: {
        fetchKnowledge(state) {
            return new Promise((res) => {
                fetch('/api/knowledge').then(response => response.json()).then(data => {
                    state.knowledge = data;
                    res();
                });
            });
        }
    },
    actions: {},
    getters: {}
}