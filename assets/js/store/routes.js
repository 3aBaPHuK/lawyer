export default {
    routes: [],
    mutations: {
        fillRoutes(state) {
            state.routes.routes = state.pages.pages.map(page => {
                return {
                    path: page.path,
                    component: page.component
                }
            })
        },
    }
}