require('../css/app.css');


import Vue from 'vue';
import AppComponent from './src/App.vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import Articles from './store/articles';
import Pages from './store/pages';
import Reviews from './store/reviews';
import Contacts from './store/contacts';
import Knowledge from './store/knowledge';
import Categories from './store/categories';
let SocialSharing = require('vue-social-sharing');


Vue.component('app-component', AppComponent);

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(SocialSharing);


String.prototype.stripTags = function () {
    let tmp       = document.createElement("DIV");
    tmp.innerHTML = this;

    return tmp.innerText;
};


const store = new Vuex.Store({
    modules: {
        pages: Pages,
        articles: Articles,
        reviews: Reviews,
        contacts: Contacts,
        knowledge: Knowledge,
        categories: Categories,
    },
    getters: {
        categoryByKnowledgeId: state => id => {
            return state.categories.categories.find(category => {
                return !!category.knowledge.filter(know => parseInt(know.id) === parseInt(id)).length
            });
        },
        articlesByPage: state => name => {
            return state.pages.pages.find(page => page.component === name).articles;
        },
        pageByRoute: state => route => {
            return state.pages.pages.find(page => page.route === route)
        },
        articleByName: state => name => {
            return state.articles.articles.find(article => {
                return article.name === name
            })
        },
        getKnowledgeById: state => id => {
            let knowledge = [];

            state.categories.categories.forEach((category) => {
                knowledge = knowledge.concat(category.knowledge)
            });

            return knowledge.find(item => item.id === parseInt(id));
        },
        categoryById: state => id => {
            return state.categories.categories.find(category => category.id === id);
        },
        articleContentByName: state => name => {
            const article = state.articles.articles.find(article => {
                return article.name === name
            });

            if (!article) {
                return '';
            }

            return article.content;
        },
        getPages: state => state.pages.pages,
        getContacts: state => state.contacts.contacts,
        getArticles: state => state.articles.articles,
        getReviews: state => state.reviews.reviews,
        getKnowledge: state => state.knowledge.knowledge,
        getCategories: state => state.categories.categories,
    }
});

const mixin = Vue.mixin({
});

let router = new VueRouter({
    scrollBehavior: function(to, from, savedPosition) {
        if (to.hash) {
            return {selector: to.hash}
        } else {
            return { x: 0, y: 0 }
        }
    },
    hashbang: false,
});

router.beforeEach((to, from, next) => {
    next();
});

Vue.use(Vuex);

const vm = new Vue({
    mixins: [mixin],
    el: '#app',
    template: '<app-component></app-component>',
    store,
    router
});